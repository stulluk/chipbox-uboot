   // by kb :20100416
#ifndef TYPE_DEF_H
#define TYPE_DEF_H

/* Define Types */
#ifndef U32
typedef unsigned int U32;
#endif
#ifndef U16
typedef unsigned short U16;
#endif
#ifndef U8
typedef unsigned char U8;
#endif
#ifndef S32
typedef int S32;
#endif
#ifndef S16
typedef short S16;
#endif
#ifndef S8
typedef signed char S8;
#endif
#ifndef F32
typedef float F32;
#endif
#ifndef F64
typedef double F64;
#endif

#endif
